# 第一部分 简介

`JSocket`是一个简易的`Socket`封装。

# 第二部分 开始使用

使用`JSocket`可以直接下载源代码编译或者下载已经编译的`jar`文件，如果您是使用`maven`来构建项目，也可以直接在`pom.xml`中添加`JSocket`的坐标：

[![Maven central](https://maven-badges.herokuapp.com/maven-central/com.jianggujin/JSocket/badge.svg)](https://maven-badges.herokuapp.com/maven-central/com.jianggujin/JSocket)

```xml
<!-- http://mvnrepository.com/artifact/com.jianggujin/JSocket -->
<dependency>
    <groupId>com.jianggujin</groupId>
    <artifactId>JSocket</artifactId>
    <version>最新版本</version>
</dependency>
```

最新的版本可以从[Maven仓库](http://mvnrepository.com/artifact/com.jianggujin/JSocket)或者[码云](https://gitee.com/jianggujin)获取。

## 2.1 一个例子

```java
package com.jianggujin.socket.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.SocketException;

import org.junit.Test;

import com.jianggujin.socket.JSocketClient;

public class SocketClientTest {
   @Test
   public void test() throws SocketException, IOException {
      JSocketClient client = new JSocketClient();
      client.connect("www.baidu.com", 80);
      InputStream input = client.getInputStream();
      OutputStream out = client.getOutputStream();
      out.write("GET http://www.baidu.com/ HTTP/1.1\r\n".getBytes("UTF-8"));
      out.write("Accept-Encoding: gzip,deflate\r\n".getBytes("UTF-8"));
      out.write("Host: www.baidu.com\r\n".getBytes("UTF-8"));
      out.write("Connection: Keep-Alive\r\n".getBytes("UTF-8"));
      out.write("User-Agent: SocketClient\r\n\r\n".getBytes("UTF-8"));
      BufferedReader reader = new BufferedReader(new InputStreamReader(input));
      String line = null;
      // 只读取响应头信息
      while ((line = reader.readLine()) != null && line.length() > 0) {
         System.out.println(line);
      }
      reader.close();
      client.close();
   }
}
```